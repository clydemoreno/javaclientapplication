/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclientapplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaclientapplication.exceptions.PreexistingEntityException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MSi
 */
public class EventTest {
    
    public EventTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventPK method, of class Event.
     */
    @Test
    public void testGetEventPK() {
        System.out.println("getEventPK");
        Event instance = new Event();
       instance.setIntervalValue(32);
        instance.setComment("My Comment");
        EntityManagerFactory f = Persistence.createEntityManagerFactory("JavaClientApplicationPU");
        SampleJpaController jpaController = new SampleJpaController(f);
       
        Sample sample = null;
        
      
        try {
             sample = jpaController.findSample(3);
             if(sample == null){
                sample = new Sample();
                sample.setId(3);

                sample.setYear(4);
                sample.setName("My nffdfaame");
                jpaController.create(sample);
             }
             else{
               DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
               Date date = new Date();
               sample.setName(dateFormat.format(date));
               jpaController.edit(sample);
                
             }
             
             List<Sample> list = jpaController.findSampleEntities();
             
             for(Sample item : list){
                 System.out.println(item.getId());
             
            }
        
             
        } catch (PreexistingEntityException ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//       Sample s = jpaController.findSample(1);
//       assertEquals(sample.getName(), s.getName());
        
        
//@PersistenceContext
//EntityManagerFactory emf;
//EntityManager em;
//@Resource
//UserTransaction utx;
//...
//em = emf.createEntityManager();
//try {
//  utx.begin();
//  em.persist(SomeEntity);
//  em.merge(AnotherEntity);
//  em.remove(ThirdEntity);
//  utx.commit();
//} catch (Exception e) {
//  utx.rollback();
//}
//        
        
        //dbcall.persist(instance);
      // Event newInstance =  dbcall.getIntervalValue(32)
        //assertEquals(newInstance.getComment ==  instance.getComment);
        //dbcall.delete(instance);
        //assertNull(
        assertNotNull(instance);
//        EventPK expResult = null;
//        EventPK result = instance.getEventPK();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setEventPK method, of class Event.
     */
    @Test
    public void testSetEventPK() {
        System.out.println("setEventPK");
        EventPK eventPK = null;
        Event instance = new Event();
        instance.setEventPK(eventPK);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBody method, of class Event.
     */
    @Test
    public void testGetBody() {
        System.out.println("getBody");
        Event instance = new Event();
        byte[] expResult = null;
        byte[] result = instance.getBody();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBody method, of class Event.
     */
    @Test
    public void testSetBody() {
        System.out.println("setBody");
        byte[] body = null;
        Event instance = new Event();
        instance.setBody(body);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDefiner method, of class Event.
     */
    @Test
    public void testGetDefiner() {
        System.out.println("getDefiner");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getDefiner();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDefiner method, of class Event.
     */
    @Test
    public void testSetDefiner() {
        System.out.println("setDefiner");
        String definer = "";
        Event instance = new Event();
        instance.setDefiner(definer);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExecuteAt method, of class Event.
     */
    @Test
    public void testGetExecuteAt() {
        System.out.println("getExecuteAt");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getExecuteAt();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExecuteAt method, of class Event.
     */
    @Test
    public void testSetExecuteAt() {
        System.out.println("setExecuteAt");
        Date executeAt = null;
        Event instance = new Event();
        instance.setExecuteAt(executeAt);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIntervalValue method, of class Event.
     */
    @Test
    public void testGetIntervalValue() {
        System.out.println("getIntervalValue");
        Event instance = new Event();
        Integer expResult = null;
        Integer result = instance.getIntervalValue();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIntervalValue method, of class Event.
     */
    @Test
    public void testSetIntervalValue() {
        System.out.println("setIntervalValue");
        Integer intervalValue = null;
        Event instance = new Event();
        instance.setIntervalValue(intervalValue);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIntervalField method, of class Event.
     */
    @Test
    public void testGetIntervalField() {
        System.out.println("getIntervalField");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getIntervalField();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIntervalField method, of class Event.
     */
    @Test
    public void testSetIntervalField() {
        System.out.println("setIntervalField");
        String intervalField = "";
        Event instance = new Event();
        instance.setIntervalField(intervalField);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCreated method, of class Event.
     */
    @Test
    public void testGetCreated() {
        System.out.println("getCreated");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getCreated();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCreated method, of class Event.
     */
    @Test
    public void testSetCreated() {
        System.out.println("setCreated");
        Date created = null;
        Event instance = new Event();
        instance.setCreated(created);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModified method, of class Event.
     */
    @Test
    public void testGetModified() {
        System.out.println("getModified");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getModified();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModified method, of class Event.
     */
    @Test
    public void testSetModified() {
        System.out.println("setModified");
        Date modified = null;
        Event instance = new Event();
        instance.setModified(modified);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLastExecuted method, of class Event.
     */
    @Test
    public void testGetLastExecuted() {
        System.out.println("getLastExecuted");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getLastExecuted();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLastExecuted method, of class Event.
     */
    @Test
    public void testSetLastExecuted() {
        System.out.println("setLastExecuted");
        Date lastExecuted = null;
        Event instance = new Event();
        instance.setLastExecuted(lastExecuted);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStarts method, of class Event.
     */
    @Test
    public void testGetStarts() {
        System.out.println("getStarts");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getStarts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStarts method, of class Event.
     */
    @Test
    public void testSetStarts() {
        System.out.println("setStarts");
        Date starts = null;
        Event instance = new Event();
        instance.setStarts(starts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEnds method, of class Event.
     */
    @Test
    public void testGetEnds() {
        System.out.println("getEnds");
        Event instance = new Event();
        Date expResult = null;
        Date result = instance.getEnds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEnds method, of class Event.
     */
    @Test
    public void testSetEnds() {
        System.out.println("setEnds");
        Date ends = null;
        Event instance = new Event();
        instance.setEnds(ends);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStatus method, of class Event.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStatus method, of class Event.
     */
    @Test
    public void testSetStatus() {
        System.out.println("setStatus");
        String status = "";
        Event instance = new Event();
        instance.setStatus(status);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOnCompletion method, of class Event.
     */
    @Test
    public void testGetOnCompletion() {
        System.out.println("getOnCompletion");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getOnCompletion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOnCompletion method, of class Event.
     */
    @Test
    public void testSetOnCompletion() {
        System.out.println("setOnCompletion");
        String onCompletion = "";
        Event instance = new Event();
        instance.setOnCompletion(onCompletion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSqlMode method, of class Event.
     */
    @Test
    public void testGetSqlMode() {
        System.out.println("getSqlMode");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getSqlMode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSqlMode method, of class Event.
     */
    @Test
    public void testSetSqlMode() {
        System.out.println("setSqlMode");
        String sqlMode = "";
        Event instance = new Event();
        instance.setSqlMode(sqlMode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getComment method, of class Event.
     */
    @Test
    public void testGetComment() {
        System.out.println("getComment");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getComment();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setComment method, of class Event.
     */
    @Test
    public void testSetComment() {
        System.out.println("setComment");
        String comment = "";
        Event instance = new Event();
        instance.setComment(comment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOriginator method, of class Event.
     */
    @Test
    public void testGetOriginator() {
        System.out.println("getOriginator");
        Event instance = new Event();
        int expResult = 0;
        int result = instance.getOriginator();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOriginator method, of class Event.
     */
    @Test
    public void testSetOriginator() {
        System.out.println("setOriginator");
        int originator = 0;
        Event instance = new Event();
        instance.setOriginator(originator);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTimeZone method, of class Event.
     */
    @Test
    public void testGetTimeZone() {
        System.out.println("getTimeZone");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getTimeZone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTimeZone method, of class Event.
     */
    @Test
    public void testSetTimeZone() {
        System.out.println("setTimeZone");
        String timeZone = "";
        Event instance = new Event();
        instance.setTimeZone(timeZone);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCharacterSetClient method, of class Event.
     */
    @Test
    public void testGetCharacterSetClient() {
        System.out.println("getCharacterSetClient");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getCharacterSetClient();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCharacterSetClient method, of class Event.
     */
    @Test
    public void testSetCharacterSetClient() {
        System.out.println("setCharacterSetClient");
        String characterSetClient = "";
        Event instance = new Event();
        instance.setCharacterSetClient(characterSetClient);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCollationConnection method, of class Event.
     */
    @Test
    public void testGetCollationConnection() {
        System.out.println("getCollationConnection");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getCollationConnection();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCollationConnection method, of class Event.
     */
    @Test
    public void testSetCollationConnection() {
        System.out.println("setCollationConnection");
        String collationConnection = "";
        Event instance = new Event();
        instance.setCollationConnection(collationConnection);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDbCollation method, of class Event.
     */
    @Test
    public void testGetDbCollation() {
        System.out.println("getDbCollation");
        Event instance = new Event();
        String expResult = "";
        String result = instance.getDbCollation();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDbCollation method, of class Event.
     */
    @Test
    public void testSetDbCollation() {
        System.out.println("setDbCollation");
        String dbCollation = "";
        Event instance = new Event();
        instance.setDbCollation(dbCollation);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBodyUtf8 method, of class Event.
     */
    @Test
    public void testGetBodyUtf8() {
        System.out.println("getBodyUtf8");
        Event instance = new Event();
        byte[] expResult = null;
        byte[] result = instance.getBodyUtf8();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBodyUtf8 method, of class Event.
     */
    @Test
    public void testSetBodyUtf8() {
        System.out.println("setBodyUtf8");
        byte[] bodyUtf8 = null;
        Event instance = new Event();
        instance.setBodyUtf8(bodyUtf8);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hashCode method, of class Event.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Event instance = new Event();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class Event.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Event instance = new Event();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Event.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Event instance = new Event();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}