/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclientapplication;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "security_list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SecurityList.findAll", query = "SELECT s FROM SecurityList s"),
    @NamedQuery(name = "SecurityList.findBySecurityListId", query = "SELECT s FROM SecurityList s WHERE s.securityListId = :securityListId"),
    @NamedQuery(name = "SecurityList.findBySymbol", query = "SELECT s FROM SecurityList s WHERE s.symbol = :symbol"),
    @NamedQuery(name = "SecurityList.findBySecurityId", query = "SELECT s FROM SecurityList s WHERE s.securityId = :securityId"),
    @NamedQuery(name = "SecurityList.findByProduct", query = "SELECT s FROM SecurityList s WHERE s.product = :product"),
    @NamedQuery(name = "SecurityList.findBySecurityType", query = "SELECT s FROM SecurityList s WHERE s.securityType = :securityType"),
    @NamedQuery(name = "SecurityList.findByMaturityDate", query = "SELECT s FROM SecurityList s WHERE s.maturityDate = :maturityDate"),
    @NamedQuery(name = "SecurityList.findByStrikePrice", query = "SELECT s FROM SecurityList s WHERE s.strikePrice = :strikePrice"),
    @NamedQuery(name = "SecurityList.findByMinPriceIncrement", query = "SELECT s FROM SecurityList s WHERE s.minPriceIncrement = :minPriceIncrement"),
    @NamedQuery(name = "SecurityList.findByUnitOfMeasure", query = "SELECT s FROM SecurityList s WHERE s.unitOfMeasure = :unitOfMeasure"),
    @NamedQuery(name = "SecurityList.findByCapPrice", query = "SELECT s FROM SecurityList s WHERE s.capPrice = :capPrice"),
    @NamedQuery(name = "SecurityList.findByFloorPrice", query = "SELECT s FROM SecurityList s WHERE s.floorPrice = :floorPrice"),
    @NamedQuery(name = "SecurityList.findByOptPayout", query = "SELECT s FROM SecurityList s WHERE s.optPayout = :optPayout"),
    @NamedQuery(name = "SecurityList.findByUnderlyingSymbol", query = "SELECT s FROM SecurityList s WHERE s.underlyingSymbol = :underlyingSymbol"),
    @NamedQuery(name = "SecurityList.findBySecurityDesc", query = "SELECT s FROM SecurityList s WHERE s.securityDesc = :securityDesc")})
public class SecurityList implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "security_list_id")
    private Integer securityListId;
    @Column(name = "symbol")
    private String symbol;
    @Basic(optional = false)
    @Column(name = "security_id")
    private String securityId;
    @Column(name = "product")
    private Integer product;
    @Column(name = "security_type")
    private String securityType;
    @Column(name = "maturity_date")
    @Temporal(TemporalType.DATE)
    private Date maturityDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "strike_price")
    private Double strikePrice;
    @Column(name = "min_price_increment")
    private Double minPriceIncrement;
    @Column(name = "unit_of_measure")
    private String unitOfMeasure;
    @Column(name = "cap_price")
    private Double capPrice;
    @Column(name = "floor_price")
    private Double floorPrice;
    @Column(name = "opt_payout")
    private Double optPayout;
    @Column(name = "underlying_symbol")
    private String underlyingSymbol;
    @Column(name = "security_desc")
    private String securityDesc;

    public SecurityList() {
    }

    public SecurityList(Integer securityListId) {
        this.securityListId = securityListId;
    }

    public SecurityList(Integer securityListId, String securityId) {
        this.securityListId = securityListId;
        this.securityId = securityId;
    }

    public Integer getSecurityListId() {
        return securityListId;
    }

    public void setSecurityListId(Integer securityListId) {
        this.securityListId = securityListId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSecurityId() {
        return securityId;
    }

    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    public Integer getProduct() {
        return product;
    }

    public void setProduct(Integer product) {
        this.product = product;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public Double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(Double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public Double getMinPriceIncrement() {
        return minPriceIncrement;
    }

    public void setMinPriceIncrement(Double minPriceIncrement) {
        this.minPriceIncrement = minPriceIncrement;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Double getCapPrice() {
        return capPrice;
    }

    public void setCapPrice(Double capPrice) {
        this.capPrice = capPrice;
    }

    public Double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(Double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public Double getOptPayout() {
        return optPayout;
    }

    public void setOptPayout(Double optPayout) {
        this.optPayout = optPayout;
    }

    public String getUnderlyingSymbol() {
        return underlyingSymbol;
    }

    public void setUnderlyingSymbol(String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol;
    }

    public String getSecurityDesc() {
        return securityDesc;
    }

    public void setSecurityDesc(String securityDesc) {
        this.securityDesc = securityDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (securityListId != null ? securityListId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityList)) {
            return false;
        }
        SecurityList other = (SecurityList) object;
        if ((this.securityListId == null && other.securityListId != null) || (this.securityListId != null && !this.securityListId.equals(other.securityListId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaclientapplication.SecurityList[ securityListId=" + securityListId + " ]";
    }
    
}
