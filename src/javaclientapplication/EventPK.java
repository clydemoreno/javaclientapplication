/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclientapplication;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;

/**
 *
 * @author MSi
 */
@Embeddable
public class EventPK implements Serializable {
    @Basic(optional = false)
    private String db;
    @Basic(optional = false)
    private String name;

    public EventPK() {
    }

    public EventPK(String db, String name) {
        this.db = db;
        this.name = name;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (db != null ? db.hashCode() : 0);
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventPK)) {
            return false;
        }
        EventPK other = (EventPK) object;
        if ((this.db == null && other.db != null) || (this.db != null && !this.db.equals(other.db))) {
            return false;
        }
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaclientapplication.EventPK[ db=" + db + ", name=" + name + " ]";
    }
    
}
