/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclientapplication;

import java.io.Serializable;
import java.util.List;
import javaclientapplication.exceptions.NonexistentEntityException;
import javaclientapplication.exceptions.PreexistingEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author MSi
 */
public class SampleJpaController implements Serializable {

    public SampleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sample sample) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(sample);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSample(sample.getId()) != null) {
                throw new PreexistingEntityException("Sample " + sample + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sample sample) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            sample = em.merge(sample);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sample.getId();
                if (findSample(id) == null) {
                    throw new NonexistentEntityException("The sample with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sample sample;
            try {
                sample = em.getReference(Sample.class, id);
                sample.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sample with id " + id + " no longer exists.", enfe);
            }
            em.remove(sample);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sample> findSampleEntities() {
        return findSampleEntities(true, -1, -1);
    }

    public List<Sample> findSampleEntities(int maxResults, int firstResult) {
        return findSampleEntities(false, maxResults, firstResult);
    }

    private List<Sample> findSampleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sample.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sample findSample(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sample.class, id);
        } finally {
            em.close();
        }
    }

    public int getSampleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sample> rt = cq.from(Sample.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
